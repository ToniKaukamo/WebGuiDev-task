const hexToRgb = (hex) => {
    // Regex to check for correct hexvalues
    const hexRegex = /^([0-9A-Fa-f]{6})$/;
    if (hexRegex.test(hex)) {
        const red = parseInt(hex.substring(0, 2), 16);
        const green = parseInt(hex.substring(2, 4), 16);
        const blue = parseInt(hex.substring(4, 6), 16);
        return `rgb(${red}, ${green}, ${blue})`;
    } else {
        throw new Error("Invalid hex color value");
    }
}

const rgbToHex = (r,g,b) => {
    if (checkValue(r) && checkValue(g) && checkValue(b)) {
        const redHex = parseInt(r).toString(16).padStart(2, '0');
        const greenHex = parseInt(g).toString(16).padStart(2, '0');
        const blueHex = parseInt(b).toString(16).padStart(2, '0');
        const hex = `#${redHex}${greenHex}${blueHex}`;
        console.log(hex);
        return hex;
    } else {
        throw new Error("Invalid RGB values");
    }
}
const checkValue = (value) => {
    return value >= 0 && value <= 255;
}

module.exports = {
    hexToRgb,
    rgbToHex
};