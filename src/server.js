const express = require("express");
const convert = require("./converter.js");
app = express();

app.use(express.static("public"));
app.use(express.json());

app.get("/api/", (req,res) =>{
    res.send("OK");
});

app.post('/api/rgbtohex', (req, res) => {
    const { r, g, b } = req.body;
    const hex = convert.rgbToHex(r, g, b);
    res.json({ hex });
});

app.post('/api/hextorgb', (req, res) => {
    const { hex } = req.body;
    const rgb = convert.hexToRgb(hex);
    res.json({ rgb });
});

module.exports = app;