const rbgToHex  = async () => {
    const red = document.getElementById('red').value;
    const green = document.getElementById('green').value;
    const blue = document.getElementById('blue').value;

    const response = await fetch('/api/rgbtohex', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ r: red, g: green, b: blue })
    });
    if (response.ok) {
        const data = await response.json();
        document.getElementById('result-hex').textContent = `Hex: ${data.hex}`;
    } else {
        const errorData = await response.json();
        document.getElementById('result-hex').textContent = `Error: ${errorData.error}`;
    }
}
const hexToRgb = async () => {
    const hex = document.getElementById('hex').value;
    const response = await fetch('/api/hextorgb', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ hex })
    });
    if (response.ok) {
        const data = await response.json();
        document.getElementById('result-rgb').textContent = `Hex: ${data.rgb}`;
    } else {
        const errorData = await response.json();
        document.getElementById('result-rgb').textContent = `Error: ${errorData.error}`;
    }
}
