#!/bin/bash

npm init -y
git init 

mkdir src
touch src/main.js
touch src/routes.js

mkdir publichtml
touch publichtml/index.html

mkdir test
touch test/ui.spec.js

npm i --save express
npm i --save-dev selenium-webdriver mocha chai
